import telebot
import argparse
import requests
import threading
import time

backup_servise_url = "http://pgbackups:8080/"
alert_chenal ="-1001489497222"

parser = argparse.ArgumentParser()
parser.add_argument('-t')
args = parser.parse_args()

bot = telebot.TeleBot(args.t)

def thread_function():
    while True:
        r = requests.get(backup_servise_url)
        print(r.json())
        if r.json()['Last']['Exit_status'] == 0:
            msg = """
✅ Бекап базы выполнен:

{stdout}
Starting Time: {StartingTime}
Exit Time: {ExitTime}
                    """.format(stdout=r.json()['Last']['Stdout'], ExitTime=r.json()['Last']['ExitTime'],
                               StartingTime=r.json()['Last']['StartingTime'])

            bot.send_message(alert_chenal, msg)
        elif r.json()['Last']['Exit_status'] != 0:
            msg = """
❗Бекап базы не выполнен'
{Stderr}
            """.format(Stderr=r.json()['Last']['Stderr'])
            bot.send_message(alert_chenal, msg)
        time.sleep(28800)

x = threading.Thread(target=thread_function, args=())
x.start()

@bot.message_handler(commands=['start', 'stop'])
def start_message(message):
    bot.send_message(message.chat.id, 'нан')

@bot.channel_post_handler(content_types=['text'])
def send_text(message):
    if message.text.lower() == 'backup_status':
        r = requests.get(backup_servise_url)
        print(r.json())
        if r.json()['Last']['Exit_status'] == 0:
            msg= """
✅ Бекап базы выполнен:

{stdout}
Starting Time: {StartingTime}
Exit Time: {ExitTime}
            """.format(stdout=r.json()['Last']['Stdout'], ExitTime=r.json()['Last']['ExitTime'], StartingTime=r.json()['Last']['StartingTime'])
            bot.send_message(message.chat.id, msg)
        elif r.json()['Last']['Exit_status'] != 0:
            bot.send_message(message.chat.id, 'Бекап базы не выполнен')
    elif message.text.lower() == 'пока':
        bot.send_message(message.chat.id, 'Прощай, создатель')

@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.text.lower() == 'backup_status':
        r = requests.get(backup_servise_url)
        print(r.json())
        if r.json()['Last']['Exit_status'] == 0:
            msg= """
✅ Бекап выполнен:

Stdout: {stdout}
Exit Time: {ExitTime}
Starting Time: {StartingTime}
            """.format(stdout=r.json()['Last']['Stdout'], ExitTime=r.json()['Last']['ExitTime'], StartingTime=r.json()['Last']['StartingTime'])
            bot.send_message(message.chat.id, msg)
        elif r.json()['Last']['Exit_status'] != 0:
            bot.send_message(message.chat.id, 'Бекап базы не выполнен')
    elif message.text.lower() == 'пока':
        bot.send_message(message.chat.id, 'Прощай, создатель')
bot.polling()
